﻿#include "stdafx.h"
#include <Windows.h>
#include <fstream>
#include <locale>
#include <codecvt>
#include "Pathcch.h"
#include "Shlwapi.h"
#include "DLL.h"
using namespace std;

HINSTANCE		hinstLib;
DATA			*pData;
HANDLE			hMapObject;

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	hinstLib = hModule;	

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		hMapObject = CreateFileMapping((HANDLE)0xFFFFFFFF,	NULL,PAGE_READWRITE,0,sizeof(DATA),	L"MyHook Sample");

		bool isExisted;
		isExisted = (GetLastError() == ERROR_ALREADY_EXISTS);

		if (hMapObject == NULL)  return FALSE;

		pData = (DATA *)MapViewOfFile(hMapObject, FILE_MAP_WRITE, 0, 0, 0);

		if (pData == NULL)  return FALSE;

		if (!isExisted) {
			ZeroMemory(pData->buffer, MAX_BUFFER);
			pData->n = 0;
		}

		// Write down name of the current module
		WCHAR szFullPathName[255], *szFileName;
		wcscat_s(pData->buffer, L"Hook was attached to:");
		GetModuleFileName(NULL, szFullPathName, 255);
		szFileName = PathFindFileName(szFullPathName);
		wcscat_s(pData->buffer, szFileName);
		wcscat_s(pData->buffer, L"\n");
		pData->n += 22 + wcslen(szFileName);
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		// write log data to file
		GetModuleFileName(NULL, szFullPathName, 255);
		PathCchRemoveFileSpec((LPTSTR)szFullPathName, 255);
		wcscat_s(szFullPathName, L"/log-data.txt");
		wfstream f;
		f.open(szFullPathName, ios::out);
		const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
		f.imbue(utf8_locale);
		f << pData->buffer;
		f.close();
		UnmapViewOfFile(pData);
		CloseHandle(hMapObject);
		break;
	}
	return TRUE;
}

