#pragma once

#ifndef DLL_HEADER
#define DLL_HEADER

#define MAX_BUFFER	2048

typedef struct {
	WCHAR	buffer[MAX_BUFFER];	
	int		n;					
								
} DATA;

#endif
