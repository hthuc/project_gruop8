﻿#include "stdafx.h"
#include <Windowsx.h>
#include "DLL.h"

#define EXPORT  __declspec(dllexport)

extern HINSTANCE hinstLib;
extern DATA		*pData;

HHOOK hHook = NULL;


LRESULT CALLBACK KeyboardHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0)	// do not process message 
		return CallNextHookEx(hHook, nCode, wParam, lParam);

	if ((nCode == HC_ACTION) && (wParam == WM_KEYUP))
		if (pData->n == MAX_BUFFER)
			MessageBox(GetFocus(), L"Buffer is overflow", L"Error", MB_OK);
		else
		{
			PKBDLLHOOKSTRUCT p = (PKBDLLHOOKSTRUCT)lParam;
			pData->buffer[pData->n] = (WCHAR)p->vkCode;
			pData->n++;
			pData->buffer[pData->n] = 0;
		}
	return CallNextHookEx(hHook, nCode, wParam, lParam);
}

EXPORT void _doInstallHook(HWND hWnd)
{
	if (hHook!=NULL) return;

	hHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC) KeyboardHookProc, hinstLib, 0);
	if (hHook)
		MessageBox(hWnd, L"Setup hook successfully", L"Result", MB_OK);
	else
		MessageBox(hWnd, L"Setup hook fail", L"Result", MB_OK);
}

EXPORT void _doRemoveHook(HWND hWnd)
{
	if (hHook==NULL) 
		return;
	UnhookWindowsHookEx(hHook);
	hHook = NULL;
	MessageBox(hWnd, L"Remove hook successfully", L"Result", MB_OK);
}