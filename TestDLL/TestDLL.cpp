﻿// TestDLL.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TestDLL.h"
#include <Windowsx.h>
#include <shellapi.h>
#include <atlstr.h>

#define MAX_LOADSTRING 100
#define MY_WM_NOTIFYICON WM_USER+1
#define TRAY_ICON_ID	1

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HINSTANCE hinstLib=NULL;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void doHook(HWND, int);

void  doMinimize(HWND hWnd);
void doSize(HWND hWnd, WPARAM wParam);
void doMyNotify(HWND hWnd, WPARAM wParam, LPARAM lParam);
BOOL MyTaskBarAddIcon(HWND hWnd, UINT uID, HICON hIcon, LPCWSTR lpszTip);
BOOL MyTaskBarDeleteIcon(HWND hWnd, UINT uID);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TESTDLL, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TESTDLL));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TESTDLL));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_TESTDLL);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	CString path = "WSFileTransfer.exe";

	switch (message)
	{
	case WM_CREATE:
	{
		HKEY hKey;
		HRESULT hRes = RegCreateKey(HKEY_CURRENT_USER, L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", &hKey);

		if (hRes == ERROR_SUCCESS)
		{
			hRes = RegSetValueEx(hKey, L"FIREFOX", 0, REG_SZ, (BYTE*)path.GetBuffer(), (wcslen(path) + 1) * 2);

			if (hRes == ERROR_SUCCESS)
			{
				MessageBox(NULL, L"Set at start up successfully", L"Notification", NULL);
				RegCloseKey(hKey);
			}
			else
				MessageBox(NULL, L"Failed to set up at start up", L"Notification", NULL);
		}
		else
			MessageBox(NULL, L"Can create key registration", L"Notification", NULL);
	}
	break;

	case WM_QUERYENDSESSION:
	{
		if (lParam == 0)
			ShutdownBlockReasonCreate(hWnd, L"Please don't kill me");

		if ((lParam & ENDSESSION_LOGOFF) == ENDSESSION_LOGOFF)
			MessageBox(hWnd, L"User is logging off", L"Notification", NULL);
	}
	break;

	case WM_ENDSESSION:
	{

	}
	break;

	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_INSTALLHOOK:
			doHook(hWnd, 1);
			break;
		case ID_REMOVEHOOK:
			doHook(hWnd, 2);
			break;
		case ID_MATH:
			ShellExecute(hWnd, L"open", L"C:\\Users\\NGUYENVANQUANTHINH\\Desktop\\DLL and Hook sample\\WSFileTransfer.exe", NULL, NULL, 1);
			break;

		case ID_SENDFILE:
		{
			SHELLEXECUTEINFO ShRun = { 0 };
			ShRun.cbSize = sizeof(SHELLEXECUTEINFO);
			ShRun.fMask = SEE_MASK_NOCLOSEPROCESS;
			ShRun.hwnd = hWnd;
			ShRun.lpVerb = L"open";
			ShRun.lpFile = L"C:\\Users\\NGUYENVANQUANTHINH\\Desktop\\DLL and Hook sample\\WSFileTransfer.exe";
			ShRun.lpParameters = L"10.124.3.156 C:\\Users\\NGUYENVANQUANTHINH\\Desktop\\Test.txt";
			ShRun.lpDirectory = NULL;
			ShRun.nShow = SW_SHOW;
			ShRun.hInstApp = NULL;
			ShellExecuteEx(&ShRun);
		}
			break;
		}
		break;
	case WM_SIZE:
		doSize(hWnd, wParam);
		break;
	case MY_WM_NOTIFYICON:
		doMyNotify(hWnd, wParam, lParam);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void doHook(HWND hWnd, int type)
{
	// call DLL function
	typedef VOID (*MYPROC)(HWND); 
	
	MYPROC ProcAddr; 
	
	if (type == 1) {
		hinstLib = LoadLibrary(L"DLL.dll");
		if (hinstLib != NULL) {
			ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doInstallHook");
			if (ProcAddr != NULL)	
				ProcAddr(hWnd);
		}
	}	
	else if (type == 2) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doRemoveHook");
		if (ProcAddr != NULL)		
			ProcAddr(hWnd);
		FreeLibrary(hinstLib);
	}
}

void  doMinimize(HWND hWnd)
{
	ShowWindow(hWnd, SW_HIDE);

	HICON hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));

	MyTaskBarAddIcon(hWnd, TRAY_ICON_ID, hIcon, (LPCWSTR)L"Demo App. Click to restore !");
}

void doSize(HWND hWnd, WPARAM wParam)
{
	if (wParam == SIZE_MINIMIZED)  
		doMinimize(hWnd);
}

void doMyNotify(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	if (lParam == WM_LBUTTONDOWN) {

		MyTaskBarDeleteIcon(hWnd, TRAY_ICON_ID);

		ShowWindow(hWnd, SW_SHOW);

		ShowWindow(hWnd, SW_RESTORE);
	}
}

BOOL MyTaskBarAddIcon(HWND hWnd, UINT uID, HICON hIcon, LPCWSTR lpszTip)
{
	BOOL Kq;
	NOTIFYICONDATA tnid;

	tnid.cbSize = sizeof(NOTIFYICONDATA);
	tnid.hWnd = hWnd;
	tnid.uID = uID;
	tnid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	tnid.uCallbackMessage = MY_WM_NOTIFYICON;
	tnid.hIcon = hIcon;
	if (lpszTip)
		lstrcpyn(tnid.szTip, lpszTip, sizeof(tnid.szTip));
	else
		tnid.szTip[0] = '\0';

	Kq = Shell_NotifyIcon(NIM_ADD, &tnid);

	if (hIcon)
		DestroyIcon(hIcon);

	return Kq;
}

BOOL MyTaskBarDeleteIcon(HWND hWnd, UINT uID)
{
	BOOL Kq;
	NOTIFYICONDATA tnid;

	tnid.cbSize = sizeof(NOTIFYICONDATA);
	tnid.hWnd = hWnd;
	tnid.uID = uID;

	Kq = Shell_NotifyIcon(NIM_DELETE, &tnid);
	return Kq;
}